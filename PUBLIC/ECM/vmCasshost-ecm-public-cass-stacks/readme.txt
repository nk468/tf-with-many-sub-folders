Po deploy-u maszyn, parametry:
a) cluster_name - nazwa klastra (unikalna w całej infrastrukturze). Jeżeli wartość będzie dotyczyć istniejącego już klastra, to postawione maszyny dopną się do tego klastra a nie utworzą nowy
b) seeds - adresy pierwszych 2 maszyn klastra (w formie "IP1,IP2")



1) w pliku /etc/cassandra/conf/cassandra.yaml token $$CLUSTER_NAME$$ ustawić wartością a)
2) w pliku /etc/cassandra/conf/cassandra.yaml token $$SEEDS$$ ustawić wartością b)
3) wykonać polecenie sudo systemctl start cassandra
