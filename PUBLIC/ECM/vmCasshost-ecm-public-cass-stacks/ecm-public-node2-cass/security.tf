resource "oci_core_network_security_group" "allow_ssh_custom_image" {
  compartment_id = "${var.compartment_ocid}"
  vcn_id         = "${var.ocid_vcn}"
  display_name   = "Allow SSH security group for VM based on custom image"
}

resource "oci_core_network_security_group_security_rule" "ssh_custom_image_ingress" {
  network_security_group_id = "${oci_core_network_security_group.allow_ssh_custom_image.id}"

  description = "SSH"
  direction   = "INGRESS"
  protocol    = 6 ## ICMP ("1"), TCP ("6"), UDP ("17"), and ICMPv6 ("58");
  source_type = "CIDR_BLOCK"
  source      = "0.0.0.0/0"

  tcp_options {
    destination_port_range {
      min = 22
      max = 22
    }
    source_port_range {
      min = 22
      max = 22
    }
  }
}

resource "oci_core_network_security_group_security_rule" "ssh_custom_image_egress" {
  network_security_group_id = "${oci_core_network_security_group.allow_ssh_custom_image.id}"

  description      = "SSH"
  direction        = "EGRESS"
  protocol         = 6 ## ICMP ("1"), TCP ("6"), UDP ("17"), and ICMPv6 ("58");
  destination_type = "CIDR_BLOCK"
  destination      = "0.0.0.0/0"

  tcp_options {
    destination_port_range {
      min = 22
      max = 22
    }
    source_port_range {
      min = 22
      max = 22
    }
  }
}
