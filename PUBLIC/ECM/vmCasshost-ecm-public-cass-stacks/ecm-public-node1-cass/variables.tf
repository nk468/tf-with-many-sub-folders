variable "instance_shape" {  }

variable "memory_in_gbs" {
  default = "4"
}

variable "ocpus" {
  default = "1"
}

variable "instance_source_details_boot_volume_size_in_gbs" {
  default = "50"
}
#Change:
variable "instance_display_name" {
  default = "ecm-public-n1-cass"
}

#Change:
variable "instance_hostname" {
  default = "ecm-public-n1-cass"
}

#Change: CI-Cassandra4Template01
variable "custom_image_ocid" {
  default = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaahtgzh3gxr3gfiznw5kvxatimju4c4pb4b2bsm7vpdcv2pmxwh5zq"
}

#Change: PUBLIC
variable "compartment_ocid" {
  default = "ocid1.compartment.oc1..aaaaaaaanaxstogjcnwgxnf4nztgomnf6lzapv3yyb5pvjmha4zyfaf3pceq"
}

#Change: cassdb
variable "ocid_subnet" {
  default = "ocid1.subnet.oc1.eu-frankfurt-1.aaaaaaaaxnylud73wrdxlekyzge4grtpnwp7jfeplvczh5k7g7iwmwh3snfa"
}

#Change: PUBLIC-VCN
variable "ocid_vcn" {
  default = "ocid1.vcn.oc1.eu-frankfurt-1.amaaaaaaamq2byialj7lzispthqdsayh5igv7c4kbllggz7kyckmswhmglgq"
}

variable "assign_pub_ip" {
  default = false
}

variable "source_type" {  }

#Change: BlockVolumeBackupPolicy01
variable "bv_backup_policy_id" {
  default = "ocid1.volumebackuppolicy.oc1.eu-frankfurt-1.aaaaaaaa6nomggrpw5hw5cgyalnnwfcvp7kqheeoifrir2kd7mi7lyllyaea"
}

variable "volume_display_name" {
  default = "ecm-public-n1-cass-bv"
}

variable "volume_size_in_gbs" {
  default = "100"
}

variable "block_volume_replicas_deletion" {
  default = true
}

variable "volume_source_details_type" {  }

#Change: BackupBV-CassandraTemplate01
variable "volume_source_backup_id" {
  default = "ocid1.volumebackup.oc1.eu-frankfurt-1.abtheljrdhzqqlif2yz4qeei2aeyji4h2bd4emljoiow5v4iup6zo6mpwcxa"
}

variable "volume_attachment_is_read_only" {
  default = false
}

variable "volume_attachment_is_shareable" {
  default = false
}

variable "volume_attachment_type" {  }

variable "region" {  }

variable "tenancy_ocid" {  }
