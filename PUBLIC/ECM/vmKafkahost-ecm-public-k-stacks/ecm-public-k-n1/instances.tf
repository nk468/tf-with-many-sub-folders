resource "oci_core_instance" "custom_image" {
  availability_domain = "${data.oci_identity_availability_domain.ad3.name}"
  compartment_id      = "${var.compartment_ocid}"
  shape               = "${var.instance_shape}"
  shape_config {
    memory_in_gbs = "${var.memory_in_gbs}"
    ocpus         = "${var.ocpus}"
  }

  create_vnic_details {
    subnet_id        = "${var.ocid_subnet}"
    assign_public_ip = "${var.assign_pub_ip}"
    hostname_label   = "${var.instance_hostname}"
    nsg_ids = [
      "${oci_core_network_security_group.allow_ssh_custom_image.id}"
    ]
  }

  display_name   = "${var.instance_display_name}"

  metadata = {
    user_data           = "${base64encode(file("./userdata/bootstrap"))}"
    ssh_authorized_keys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKRZ/5hyEOXJS3KCYHFB3Hg+aRCpF2RN5M6LuiI7QLCQSsvaltattK3/j5G/O/hhm2oy1DRx2GKMH/uKnPx3Hi24vI8ggfC4LE/OcccaSIDDO0NXcSz425U1mjPE+b67vnj1O37Wtn6BeLe92m+CvI6yG1eVfE5wZgtT2ES+n81FEWcx6yEHSZXSLEyn7VdIcNaL8FHhUYsLNF5tg2mZUPPyimJme5UyUY2/sm5yWKrfDaT4qgedwXbP/GkX7weqZgI7dwQDJCiLY89INQOjBqBnRtETzpBwa95PKYoFFLlrMFoCHEovuKUIPJkJEg/3OhnyA++pa+cLXYsoDg7zVj shenlong23@nbpszelagowski"
  } 

  source_details {
    source_type = "${var.source_type}"
    source_id   = "${var.custom_image_ocid}"

    boot_volume_size_in_gbs = "${var.instance_source_details_boot_volume_size_in_gbs}"
  }

  preserve_boot_volume = false
  ## Specifies whether to delete or preserve the boot volume when terminating an instance. The default value is false.
  ## Note: This value only applies to destroy operations initiated by Terraform.
}
