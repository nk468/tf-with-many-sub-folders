variable "instance_shape" {  }

variable "memory_in_gbs" {
  default = "8"
}

variable "ocpus" {
  default = "1"
}

variable "instance_source_details_boot_volume_size_in_gbs" {
  default = "50"
}
#Change:
variable "instance_display_name" {
  default = "ecm-public-k-n1"
}

#Change:
variable "instance_hostname" {
  default = "ecm-public-k-n1"
}

#Change: CI-KafkaTemplate01
variable "custom_image_ocid" {
  default = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaa3m7emfljudorluj5rp2huvtzmoaaak2htfj2vq6tqyo4yva27p3q"
}

#Change: PUBLIC
variable "compartment_ocid" {
  default = "ocid1.compartment.oc1..aaaaaaaanaxstogjcnwgxnf4nztgomnf6lzapv3yyb5pvjmha4zyfaf3pceq"
}

#Change: streaming
variable "ocid_subnet" {
  default = "ocid1.subnet.oc1.eu-frankfurt-1.aaaaaaaapigy6w5i6cm5ze3sp577i5niquqmap2nrpllisgdxwtb7drreqaq"
}

#Change: PUBLIC-VCN
variable "ocid_vcn" {
  default = "ocid1.vcn.oc1.eu-frankfurt-1.amaaaaaaamq2byialj7lzispthqdsayh5igv7c4kbllggz7kyckmswhmglgq"
}

variable "assign_pub_ip" {
  default = false
}

variable "source_type" {  }

#Change: BlockVolumeBackupPolicy01
variable "bv_backup_policy_id" {
  default = "ocid1.volumebackuppolicy.oc1.eu-frankfurt-1.aaaaaaaa6nomggrpw5hw5cgyalnnwfcvp7kqheeoifrir2kd7mi7lyllyaea"
}

variable "volume_display_name" {
  default = "ecm-public-k-n1"
}

variable "volume_size_in_gbs" {
  default = "100"
}

variable "block_volume_replicas_deletion" {
  default = true
}

variable "volume_source_details_type" {  }

#Change: BackupBV-KafkaTemplate01
variable "volume_source_backup_id" {
  default = "ocid1.volumebackup.oc1.eu-frankfurt-1.abtheljrr3sganaqovi6tqsvjizjwazczsyec5ppsojvd5jdz67inrhaiqpa"
}

variable "volume_attachment_is_read_only" {
  default = false
}

variable "volume_attachment_is_shareable" {
  default = false
}

variable "volume_attachment_type" {  }

variable "region" {  }

variable "tenancy_ocid" {  }
