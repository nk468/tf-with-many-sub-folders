resource "oci_core_volume" "custom_volume" {
  compartment_id      = "${var.compartment_ocid}"
  availability_domain = "${data.oci_identity_availability_domain.ad3.name}"
  backup_policy_id    = "${var.bv_backup_policy_id}"

  display_name = "${var.volume_display_name}"

  size_in_gbs  = "${var.volume_size_in_gbs}"

  source_details {
    id   = "${var.volume_source_backup_id}"
    type = "${var.volume_source_details_type}"
  }

  block_volume_replicas_deletion = "${var.block_volume_replicas_deletion}"
}

resource "oci_core_volume_attachment" "custom_volume_attachment" {
  attachment_type = "${var.volume_attachment_type}"
  instance_id     = "${oci_core_instance.custom_image.id}"
  volume_id       = "${oci_core_volume.custom_volume.id}"

  display_name    = "${var.volume_display_name}"

  is_read_only    = "${var.volume_attachment_is_read_only}"
  is_shareable    = "${var.volume_attachment_is_shareable}"
}

#resource "time_sleep" "wait_10_minutes" {
#  depends_on = [
#    oci_core_instance.custom_image,
#    oci_core_volume_attachment.custom_volume_attachment,
#  ]
#
#  create_duration = "5m"
#}
#
#resource "null_resource" "remote-exec" {
#  depends_on = [time_sleep.wait_10_minutes]
#
#  provisioner "remote-exec" {
#    connection {
#      agent       = false
#      timeout     = "10m"
#      type        = "ssh"
#      host        = "${oci_core_instance.custom_image.private_ip}"
#      user        = "opc"
#      private_key = "${tls_private_key.public_private_key_pair.private_key_pem}"
#    }
#
#    inline = [
#      "sudo systemctl restart sshd",
#      "sudo systemctl start dbora",
#      "sudo lsnrctl start",
#      "sudo iscsiadm -m node -o new -T ${oci_core_volume_attachment.custom_volume_attachment.iqn} -p ${oci_core_volume_attachment.custom_volume_attachment.ipv4}:${oci_core_volume_attachment.custom_volume_attachment.port}",
#      "sudo iscsiadm -m node -o update -T ${oci_core_volume_attachment.custom_volume_attachment.iqn} -n node.startup -v automatic",
#      "sudo iscsiadm -m node -T ${oci_core_volume_attachment.custom_volume_attachment.iqn} -p ${oci_core_volume_attachment.custom_volume_attachment.ipv4}:${oci_core_volume_attachment.custom_volume_attachment.port} -l",
#    ]
#  }
#}
